/* ***************************************************************************
 *
 *                              KisSplice
 *      de-novo calling alternative splicing events from RNA-seq data.
 *
 * ***************************************************************************
 *
 * Copyright INRIA
 *  contributors :  Vincent Lacroix
 *                  Pierre Peterlongo
 *                  Gustavo Sacomoto
 *                  Vincent Miele
 *                  Alice Julien-Laferriere
 *                  David Parsons
 *
 * pierre.peterlongo@inria.fr
 * vincent.lacroix@univ-lyon1.fr
 *
 * This software is a computer program whose purpose is to detect alternative
 * splicing events from RNA-seq data.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include <stdio.h>
#include "CGraph.h"
#include "BubbleEnumeration.h"

// Functor that print the bc on file
class Printer 
{
 private:
  FILE * _out_file;
 public:
  Printer(FILE * out_file)
  : _out_file(out_file) {}
  void operator()(FILE* in_file, int written_lines){
    char* buffer = new char[100 * MAX];      
    for (int i = 0; i < written_lines ; i++) {
      fgets(buffer, 100 * MAX, in_file);
      fputs(buffer, _out_file);
    }
    delete[] buffer;
  }
};

int main( int argc, char** argv )
{
  if ( argc < 8 ){
    fprintf( stderr, "Usage: %s infofile contents_file_edges contents_file_nodes basename_edges basename_nodes number_to_read output_file_edges output_file_nodes \n", argv[0] );
  exit( EXIT_FAILURE );
  }

  FILE *output_file_edge = fopen(argv[7], "w");
  //if (output_file_edge == NULL) { fprintf(stderr, "Problem opening %s!\n", argv[7]); exit(0); } 
  FILE *output_file_node = fopen(argv[8], "w");
  //if (output_file_node == NULL) { fprintf(stderr, "Problem opening %s!\n", argv[8]); exit(0); }  

  int required_sequence = atoi( argv[6] );

  Printer edgeprinter(output_file_edge);
  Printer nodeprinter(output_file_node);
  read_edges_and_nodes_withoptimIO<Printer,Printer>
    (argv[1],argv[2],argv[3],argv[4],argv[5],&required_sequence, edgeprinter, nodeprinter);
  fclose(output_file_edge);
  fclose(output_file_node);
}
