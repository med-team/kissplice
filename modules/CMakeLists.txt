add_executable(ks_clean_duplicates CleanDuplicates.cpp CycleCompression.cpp NGraph.cpp CGraph.cpp LabelledCEdge.cpp Utils.cpp)
set_property(TARGET ks_clean_duplicates PROPERTY RUNTIME_OUTPUT_DIRECTORY "${BUILD_INTERNAL_BINDIR}")
install(TARGETS ks_clean_duplicates RUNTIME DESTINATION "${RELATIVE_INTERNAL_BINDIR}")

add_executable(ks_bubble_enumeration  BubbleEnumeration.cpp CycleCompression.cpp NGraph.cpp CGraph.cpp LabelledCEdge.cpp Utils.cpp)
set_property(TARGET ks_bubble_enumeration PROPERTY RUNTIME_OUTPUT_DIRECTORY "${BUILD_INTERNAL_BINDIR}")
install(TARGETS ks_bubble_enumeration RUNTIME DESTINATION "${RELATIVE_INTERNAL_BINDIR}")

add_executable(ks_run_modules run.cpp CycleCompression.cpp NGraph.cpp CGraph.cpp LabelledCEdge.cpp SplitBcc.cpp Utils.cpp)
set_property(TARGET ks_run_modules PROPERTY RUNTIME_OUTPUT_DIRECTORY "${BUILD_INTERNAL_BINDIR}")
install(TARGETS ks_run_modules RUNTIME DESTINATION "${RELATIVE_INTERNAL_BINDIR}")

add_executable(ks_error_removal ErrorRemoval.cpp LabelledCEdge.cpp Utils.cpp)
set_property(TARGET ks_error_removal PROPERTY RUNTIME_OUTPUT_DIRECTORY "${BUILD_INTERNAL_BINDIR}")
install(TARGETS ks_error_removal RUNTIME DESTINATION "${RELATIVE_INTERNAL_BINDIR}")

add_executable(ks_print_bcc PrintBcc.cpp Utils.cpp)
set_property(TARGET ks_print_bcc PROPERTY RUNTIME_OUTPUT_DIRECTORY "${BUILD_INTERNAL_BINDIR}")
install(TARGETS ks_print_bcc RUNTIME DESTINATION "${RELATIVE_INTERNAL_BINDIR}")