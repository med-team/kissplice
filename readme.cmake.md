Some useful information for developers about CMake.

Cmake has some tutorials but they are not great, like https://cmake.org/cmake/help/latest/guide/tutorial/index.html
`man cmake` has some overviews.

Cmake steps are usually :
- *configure* : setup a temporary *build directory* with a specific build configuration, perform platform checks (for dependencies)
- *build* : compile the code / doc / ... in the given configuration
- optional *test* : run tests to check if the build is ok
- *install* : copy the generated files from the temporary *build* directory to the final install directory

# Configure
Configure command
```bash
cmake -S source_directory/ -B build_directory/ <build options...>
# source directory is the cloned git root
# build directory can be placed anywhere, but very often found at <git root>/build/
```

Options are usually all given at once to the configure command.
That are *cached* in the build directory so they can be updated, one by one or many at once.
```bash
cmake -S . -B build/ -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=~/.local/
cmake -S . -B build/ -DCMAKE_BUILD_TYPE=Release # updates CMAKE_BUILD_TYPE but keeps the other from before
```

Cmake options:
- `-DCMAKE_BUILD_TYPE=Debug|Release|...` : presets of debug / optimisation / warning compiler flags
- `-DCMAKE_INSTALL_PREFIX=path/` : during install, install to `path/{bin,lib,share}` instead of `/usr/{bin,lib,share}`. Also look at path when checking for libraries or executable dependencies.
- `-DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++` : override compiler

Kissplice defined options
- `USER_GUIDE=ON|OFF` : optionally build a pdf guide (requires latex distribution).
- `USE_BUNDLED_BCALM=ON|OFF` : by default, requires `bcalm` to be installed (system wide or in `CMAKE_INSTALL_PREFIX`). For convenience you can set this to `ON` to compile alocal bcalm and install it alongside kissplice.

You can reset the configuration / build cache by deleting the build directory.

# Build
```bash
cmake --build build_directory/
# Useful option : -jN to use up to N cpu cores in parallel.
```

# Test
```bash
# No "modern" cmake command, must run make in build dir directly
cd build_directory/
make test
# Alternative that prints test output on failure :
ctest --output-on-failure
```

Kissplice also has a sample dataset :
```bash
build_directory/bin/kissplice -r sample_example/reads1.fa -r sample_example/reads2.fa -v
```

# Install
```bash
# Install to the directory selected by CMAKE_INSTALL_PREFIX at configure time
cmake --install build_directory/
# Alternative : override the target directory, but less reliable for some projects
cmake --install build_directory/ --prefix override_target/
```

# Other stuff
## Generating documentation for developers
```bash
cd doc/dev/
make
firefox index.html
```

## Profiling
Use https://perf.wiki.kernel.org/ on a binary with debug symbols (build type = Debug or RelWithDebInfo).
Profiling build type has been removed as gprof is inconvenient to use, but if you still want to use it manually add `-pg` to C/CXX flags.
