#!/usr/bin/env python3
from sys import exit
import shutil
import shlex
from subprocess import Popen, PIPE, STDOUT


def run(command_line, shell=False):
    print(command_line)
    args = shlex.split(command_line)
    command = Popen(args, stdout=PIPE, stderr=PIPE)
    res = command.communicate()
    if len(res[1]):
        print("Integration test error : ",res[1])
        exit()
    else:
        return res[0]