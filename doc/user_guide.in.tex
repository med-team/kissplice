\documentclass[english, a4paper, 12pt]{article}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}
\usepackage{geometry}
\usepackage[english]{babel}
\usepackage{multicol}
\usepackage{babel}
\usepackage{multicol}
\usepackage{babel}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[dvips]{graphicx}
\usepackage{color}
\usepackage{alltt}
%\usepackage{dsfont}
\usepackage{amsthm}
\usepackage{enumerate}
\usepackage{hyperref}
\usepackage{fancyvrb}
\usepackage{listings}
\lstset{
    basicstyle=\small\ttfamily,
    columns=flexible,
    breaklines=true
}
\geometry{verbose,letterpaper,tmargin=2cm,bmargin=3cm,lmargin=2.5cm,rmargin=2.5cm}
\newcommand{\soft}{\texttt{KisSplice}}
\newcommand{\esoft}{\texttt{KisSplice} }

\title{
\soft\\
De-novo calling alternative splicing events\\ from RNA-seq data\\
\vspace{0.5cm}
User's guide, version @PROJECT_VERSION@
\vspace{-1cm}
}
\date{@CONFIGURE_DATE@}

\author{}
\begin{document}

\maketitle
\vspace{-1cm}
\tableofcontents
\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\soft, at a glance}

\esoft is dedicated to {\it de-novo} calling of alternative splicing events from one or several RNA-seq datasets. In addition to splicing events, \esoft detects indels and SNPs. Data from different conditions can be co-assembled with \soft.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\esoft software package}
\subsection{CeCILL licence}
This software is governed by the CeCILL licence. Details are mentioned in the \texttt{COPYING} file.

\subsection{Reference}
If you use \esoft in a published work, please cite the following reference:\\
Sacomoto et al. KISSPLICE: de-novo calling alternative splicing events from RNA-seq data. BMC Bioinformatics 13, S5 (2012). https://doi.org/10.1186/1471-2105-13-S6-S5\\

If you use \soft, kissplice2reftranscriptome and kissDE, please cite:\\
Lopez-Maestre et al., SNP calling from RNA-seq data without a reference genome: identification, quantification, differential analysis and impact on the protein sequence, Nucleic Acids Research, Volume 44, Issue 19, 2 November 2016, Page e148\\

If you use \soft, kissplice2refgenome and kissDE, please cite:\\
Benoit-Pilven et al. Complementarity of assembly-first and mapping-first approaches for alternative splicing annotation and differential analysis from RNAseq data. Sci Rep 8, 4307 (2018). https://doi.org/10.1038/s41598-018-21770-7

\subsection{Online ressources}
The main repository is at \url{https://gitlab.inria.fr/erable/kissplice}.
It contains releases, an issue-tracker for questions or bug reports, and allows people to contribute.

Biostars forum: \url{www.biostars.org/t/kissplice/} (seems unused).

\subsection{Installation}
\esoft is written in C/C\texttt{++}/Python and is running on Mac OS X and Linux 64 bits platforms.
It can be installed using the following methods, by decreasing order of convenience.

\subsubsection{Debian/Ubuntu package}
\esoft is available as a system package in Debian/Ubuntu.
\begin{Verbatim}
apt install kissplice
kissplice --help
\end{Verbatim}

\subsubsection{Conda}
Kissplice is available in conda in the \href{https://bioconda.github.io/}{bioconda channel}.
Bioconda creates packages for Linux (x86\_64 and arm64) and MacOS (x86\_64 = intel macs before M1).

\subsubsection{Docker}
You can find the latest version of KisSplice, KisSplice2RefGenome and KissDE on \href{https://hub.docker.com/repository/docker/dwishsan/kissplice-pipeline}{Docker Hub}.

We also propose a stand-alone \href{https://hub.docker.com/repository/docker/dwishsan/kde-shiny}{Docker image} for the KissDE Shiny App for KisSplice results exploration.

\subsubsection{Binary release for Debian/Ubuntu}
A precompiled standalone binary package for ubuntu is available in the \href{https://gitlab.inria.fr/erable/kissplice/-/releases}{release page}.
\begin{Verbatim}
# Decompress downloaded archive
tar xf kissplice-binary-ubuntu-<version>.tar.gz
# Access the kissplice tool
kissplice-binary-ubuntu-<version>/bin/kissplice --help
\end{Verbatim}

\subsubsection{Compile from source}
Required dependencies :
\begin{itemize}
\item cmake >= 3.9
\item C/C++14 compiler toolchain (binutils, gcc or clang)
\item python3 to run kissplice
\item git
\end{itemize}

Download a source code archive from the latest \href{https://gitlab.inria.fr/erable/kissplice/-/releases}{release} and uncompress it.
\begin{Verbatim}
# directory where the release tar.gz was uncompressed
cd kissplice/

# Replace install_directory by the path where you want your install to be.
# If you have latex installed you can add -DUSER_GUIDE=ON to generate
# the user guide at install_directory/doc/user_guide.pdf
cmake -S . -B build/ -DCMAKE_BUILD_TYPE=Release -DUSE_BUNDLED_BCALM=ON
    -DCMAKE_INSTALL_PREFIX=install_directory

cmake --build build/ # add -jN to use up to N cpu cores in parallel
cmake --install build/

# Example of use
install_directory/bin/kissplice -r sample_example/mock1.fq -r sample_example/mock2.fq
    -r sample_example/virus1.fq -r sample_example/virus2.fq
\end{Verbatim}

If you want to build from source and install to a local directory (like \texttt{~/.local/}):
\begin{Verbatim}
# directory where the release tar.gz was uncompressed
# or where the git was cloned
cd kissplice/
cmake -S . -B build/ -DCMAKE_BUILD_TYPE=Release 
    -DCMAKE_INSTALL_PREFIX=<install directory>
cmake --build build/
cmake --install build/
\end{Verbatim}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Usage}

\subsection{Testing \esoft in a couple of minutes}
The example presented here only deals with alternative splicing events, but the format is the same for SNPs and indels.
%The output format for SNPs and indels is however similar. 
%The example that will be presented only deals with alternative splicing events. However, other types of outputs are very similar and their interpretation can be easily inferred from this case.\\

%The \texttt{sample\_example} directory contains two files  \texttt{reads1.fa} and \texttt{reads2.fa} which correspond to simulated reads from two transcripts of D. melanogaster, which differ by the inclusion of one exon. Running \esoft on this dataset should output one splicing event.

The \texttt{sample\_example} directory contains four files  \texttt{mock1.fq}, \texttt{mock2.fq}, \texttt{virus1.fq}, \texttt{virus2.fq} which correspond to RNAseq reads from the MBTD1 gene in two biological conditions: mock infection and infection by Influenza A virus. There are two biological replicates for each condition. Running \esoft on this dataset should output one splicing event.
%Once you installed \soft, you can run it with defaults parameters (\texttt{k=41}) on the two fasta files present in the \texttt{sample\_example} directory of the release:
%\begin{small}
%\begin{Verbatim}[frame=single]
%bin/kissplice -r sample_example/reads1.fa -r sample_example/reads2.fa
%less results/results_reads1_reads2_k25_coherents_type_0.fa
%\end{Verbatim}
%\end{small}
\begin{small}
\begin{Verbatim}[frame=single]
./bin/kissplice -r sample_example/mock1.fq -r sample_example/mock2.fq
 -r sample_example/virus1.fq -r sample_example/virus2.fq
less results/results_mock1_mock2_virus1_virus2_k41_coherents_type_1.fa
\end{Verbatim}
\end{small}

%The result files can be found in the results directory:\\
%\texttt{results\_reads1\_reads2\_k41\_coherents\_type\_0.fa} contains the SNP/sequencing error  and  \texttt{results\_reads1\_reads2\_k41\_coherents\_type\_1.fa} contains the exon skipping event .
%Using several \texttt{-r} can help you to co-assemble datasets. 
For our example, the output files can be found in the results directory:\\
\texttt{results\_mock1\_mock2\_virus1\_virus2\_k41\_coherents\_type\_1.fa} which contains the alternative splicing event (Fig. \ref{fig:extype1_C2}).
The other files are empty.



%In fig \ref{fig:extype0}, the content of  \texttt{results\_reads1\_reads2\_k41\_coherents\_type\_0.fa}, outputting the SNPs found by \esoft.


%The output is pairwise, representing the variation between two sequences. Here it is a one nucleotide change: $G \rightarrow T$ (letter in red) between the path called "upper path" and the path called "lower path".
%The modification is at the center of the paths of length 49. \texttt{Type0a} indicates that there is a single SNP within the sequence (\texttt{Type0b} would indicate the presence of multiple SNPs, i.e. several SNPs separated by less than $k$ nt, or a pattern created by paralogous genes).\\



%When a SNP is indicated in the type 0 file, both paths can be merged into a single sequence, replacing the letter by an $N$.
%Now, let observe  the alternative splicing event reported by KisSplice (Fig. \ref{fig:extype1}).

\begin{figure}[!h]
\includegraphics[width=1\textwidth]{MBTD1_C2}
\caption{Content of the file \texttt{results\_mock1\_mock2\_virus1\_virus2\_k41\_coherents\_type\_1.fa},  the alternative splicing event found. In red are the flanking k-mers (i.e. extremities of the flanking exons). In white is the variable part (i.e. the skipped exon).}\label{fig:extype1_C2}
\end{figure}


The output is organised in blocks of four lines, each corresponding to an AS event. Here there is just one block.
Each block is composed of two sequence variants. The upper path corresponds to the longer variant, and the lower path to the shorter variant.
The sequence of each path starts and ends with the k-mer common to both path (highlighted in red). The upper path contains an additional sequence (highlighted in white). 
In this example, the additional sequence is an alternatively skipped exon. The flanking k-mers are the extremities of the flanking exons.

The fasta header of each path additionnally contains read counts. 
When using the --counts 0 option (Figure \ref{fig:extype1_C0}), the counts are easier to understand because there is just one count for each input file. 

\begin{figure}[!h]
\includegraphics[width=1\textwidth]{MBTD1_C0}
\caption{Content of the file \texttt{results\_mock1\_mock2\_virus1\_virus2\_k41\_coherents\_type\_1.fa}, when using --counts 0 option. In red are the flanking k-mers (i.e. extremities of the flanking exons). In white is the variable part (i.e. the skipped exon).}\label{fig:extype1_C0}
\end{figure}

In the example, the longer splice variant is supported by 47 reads in mock1, 78 reads in mock2, 18 reads in virus1, 13 reads in virus 2. The shorter splice variant is supported by 2 reads in mock1, 3 reads in mock 2, 14 reads in virus1 and 15 reads in virus2. Overall, this exon is more often skipped in samples infected with IAV.



When using the --counts 2 option (default option in Kissplice-2.5.x), (Figure \ref{fig:extype1_C2}), the counts of the longer variant are split in 4 categories: reads fully contained in the variable part (S counts), reads spanning the junction with the upstream exon (AS counts), reads spanning the junction with the downstream exon (SB counts), reads spanning both junctions (ASSB counts). For this example, out of the 47 reads supporting the longer variant in mock 1: 15 support the first junction, 23 the second junction, 9 are included in the skipped exon and none support both junctions.


%In the sequence identifier, we can see that the event is in the same compound than the type 0 one (the compound is here called bcc 1).
%The output is pairwise, representing the variation between two sequences. Here it is an exon skipping :  there is an inclusion in the path called "upper path" which is not in the path called "lower path".
%There are also the counts of the two isoforms:
%\begin{itemize}
%\item inclusion isoform : 0 (for condition 1, reads1.fa file) , 50 (for condition 2, reads2.fa file)
%\item exclusion isoform : 7 , 0
%\end{itemize}

%\begin{figure}[!h]
%\includegraphics[width=1\textwidth]{alt_splicing_sample_ex}
%\caption{Content of the file \texttt{results\_reads1\_reads2\_k41\_coherents\_type\_1.fa},  the alternative splicing event found. In red is the variable part in the inclusion isoform.}\label{fig:extype1}
%\end{figure}

%The splicing event is condition-specific with the inclusion isoform covered by 50 reads in condition 2 and the exclusion isoform covered by 7 reads in condition 1.\\

% You may want to map the sequences against a reference genome if you have one to see what the output corresponds to. This can be done using blat, STAR, HiSat2 or any other RNA/DNA alignment software. This step can be automatised using 
%\begin{figure}
%\includegraphics[width=1\textwidth]{ucsc.png}
%\caption{Using the reference genome, it is possible to see that the type 1 event is an exon skipping (figure obtained using Blat and the UCSC genome browser)}
%\end{figure}

% \newpage
 
 \subsection{Common TroubleShooting}
 Before running \esoft on a large dataset, (10 conditions with 100M reads each), which takes time and memory, we advise to first run \esoft on a subset of your dataset (2 conditions with 10M reads each) and get familiar with the output.
 
 If you encounter problems running \soft, this is often due to the stack size limit. Please try increasing your stack size before running \soft. The best way to do this is setting the stack size to unlimited, if your operating system accepts this, executing the following command:
 
 \begin{small}
     \begin{Verbatim}[frame=single]
     > ulimit -s unlimited
     \end{Verbatim}
    \end{small}
    
    If your operating system does not accept the previous command, you can increase your stack size to the highest value possible. To find this value, execute:
    \begin{small}
        \begin{Verbatim}[frame=single]
        > ulimit -H -s
        \end{Verbatim}
    \end{small}
    
    And then, to set the stack size, execute:
    \begin{small}
        \begin{Verbatim}[frame=single]
        > ulimit -s <highest_value>
        \end{Verbatim}
    \end{small}
    
    If the problem persists, please do not hesitate to contact us.


\subsection{Options}
Type \texttt{kissplice -h} to see the full list of options and parameters



\subsection{Counts option }
%\esoft can use several methods to quantify the paths found in the DBG.
%The quantification depends on the \texttt{min\_overlap} parameters.
A read is used for quantification if and only if it overlaps (by at least \texttt{min\_overlap} nt) the variable part of the path.
We can distinguish various parts in the paths of an event (please see Fig. \ref{fig:quantif}):
\begin{itemize}
\item AS: junction between the left part of the path (A in green) and the variable part of the longer path (S in red). A read is in AS if it has more than \texttt{min\_overlap} nt in S and A
\item SB: junction between the right part of the path (B in blue), and the variable part of the longer path (S in red). A read is in SB if it has more than \texttt{min\_overlap} nt in S and B.
\item S:  the variable part (in red), the difference between the upper and lower path. A read is in S if it has less than \texttt{min\_overlap} nt in A or B.
\item AB: junction between A and B (left, resp. right part of the path) in the  shorter path. A read is in AB if it has more than \texttt{min\_overlap} nt in A and B.
\item ASSB: the junction AS and SB with S. A read is counted in ASSB if if has more than \texttt{min\_overlap} nt in A and B (and so if fully in S).
\end{itemize}

\begin{figure}[h!]
\includegraphics[width=1\textwidth]{quantifModel.png}
\caption{Example of quantification. In lower case, the context obtained using the \texttt{--output-context} parameter.}
\label{fig:quantif}
\end{figure}

The \texttt{--counts} options can take three values: \texttt{0,1,2}.
If does not affect the quantification itself, but changes the output.

By default, in kissplice-2.5.x, the counts option is set to 2.
This affects only splicing events.

\begin{enumerate}[start=0]
%\setcounter{enumi}{0}
\item  the total count is reported  (default behaviour) as CX
\item  the counts are written for ASX, SBX, ASSBX, ABX
\item  all the counts are reported (ASX, SBX, SX, ASSBX and ABX)
\end{enumerate}

Here X indicates the read file used for the quantification.
It is possible to retrieve the total count C using: $C  =  (AS-ASSB) + (SB-ASSB) + S + ASSB$

\subsection{Paired-end reads}
For now, no particular treatment is given to paired-end reads.
We recommend that you use them as two input files :
\begin{small}
\begin{Verbatim}[frame=single]
kissplice -r condition1/1 -r condition1/2
\end{Verbatim}
\end{small}



\section{I/O}
\subsection{Input}
\esoft may be used either directly from one or several sets of reads, or from a bi-directed de-Bruijn graph.\\
In the first case, one or several fasta/fastq files containing the reads have to be provided. In the second case, the de-Bruijn graph alone has to be provided (in dot format).
In the input fasta/q files, each read should be written on exactly two lines for fasta (one for the fasta identifier and one for the sequence) and four lines for fastq. \\
The input files should have one of the following extensions: \texttt{fq, fastq, txt} or \texttt{fasta, fa}. \esoft also handles compressed reads (\texttt{.fa.gz})

To have an example of the input files, run \esoft on the test data (provided in the directory \texttt{sample\_example}), consisting of two read files in fasta format. You will find the constructed de-Bruijn graph built by \esoft in the \esoft directory.

\subsection{Output}\label{subsec:output}
If it was not provided, a de-Bruijn graph is built for the $N$ fasta files (read) given as input; the following three files are created in the results directory (for $k=XX$):
\begin{itemize}
\item \texttt{graph\_NameReadFile1\_NameReadFile2\_...\_NameReadFileN\_kXX.nodes} for the nodes of the de-Bruijn graph
\item \texttt{graph\_NameReadFile1\_NameReadFile2\_...\_NameReadFileN\_kXX.edges} for its edges.
%\item \texttt{graph\_NameReadFile1\_NameReadFile2\_...\_NameReadFileN\_kXX.solid\_kmers\_binary\_with\_count} a binary file relevant for the option \texttt{-C }
%\item \texttt{graph\_NameReadFile1\_NameReadFile2\_...\_NameReadFileN\_kXX.counts} for the $k-mers$ count, created only if \esoft was run with option \texttt{-C} coupled with \texttt{-\-keep-counts}.
\item \texttt{graph\_NameReadFile1\_NameReadFile2\_...\_NameReadFileN\_kXX.abundance} for the $k-mers$ count of each node. 
\end{itemize}
In the node file, the 1st column is the node ID, the 2nd its (forward) sequence.

In the file describing the edges, the 1st and 2nd column are the IDs of the nodes that are connected by an edge, the 3rd column codes for the direction of the edge (FF = from the forward sequence of a node to the forward sequence of the other node, RR= from reverse to reverse, FR and RF). Note that if node A is connected with node B by an edge with direction ``FF'' (``FR''), node B is connected to node A by an edge directed ``RR'' (``FR'') and vice versa.

Moreover, six fasta files for the results are created:
\begin{enumerate}
\item \texttt{results\_NameReadFile1\_NameReadFile2\_...\_NameReadFileN\_kXX\_type\_0a.fa}: Single SNPs or sequencing substitution errors
\item \texttt{results\_NameReadFile1\_NameReadFile2\_...\_NameReadFileN\_kXX\_type\_0b.fa}: Multiple SNPs or sequencing substitution errors
\item \texttt{results\_NameReadFile1\_NameReadFile2\_...\_NameReadFileN\_kXX\_type\_1.fa}: alternative splicing events
\item \texttt{results\_NameReadFile1\_NameReadFile2\_...\_NameReadFileN\_kXX\_type\_2.fa}: inexact tandem repeats
\item \texttt{results\_NameReadFile1\_NameReadFile2\_...\_NameReadFileN\_kXX\_type\_3.fa}: short indels
\item \texttt{results\_NameReadFile1\_...\_NameReadFileN\_kXX\_type\_4 .fa}:  all others, composed by a shorter path of length $> 2k$ not being a SNP
\end{enumerate}
If the read files (option \texttt{-r}) are provided as input files, the fasta files are checked for read coherency, which means that each nucleotide of each sequence has to be covered by at least one read. If \esoft is used with option \texttt{-g} (only the de-bruijn graph is provided), a check for read coherency is not possible due to missing information about read coverage.

\begin{sloppypar}
Since the read coherency step may take a long time, \esoft preemptively outputs these six fasta files before checking for read coherency in the directory \texttt{results\_without\_read\_coherency} inside the results directory. This enables the user to access the results even before the read coherency module finishes.

\esoft also creates a summary log file in the results directory named \texttt{kissplice\_log\_summary\_(time)\_(date)\_(random\_integer)} in case the user wants to view a summary of the execution. This is not meant to be a detailed output. If you need a detailed output, please run \esoft in verbose mode (parameter \texttt{-v}) and redirect the output and error stream to a file.
\end{sloppypar}

Fasta files are organized as follows, each 4-lines groups are an event
\begin{small}
\begin{Verbatim}[frame=single]
> identifier_upper_path
sequence upper path
> identifier_lower_path
sequence lower path
\end{Verbatim}
\end{small}

The identifier for the upper path is formatted as follows :
\begin{small}
\begin{Verbatim}[frame=single]
>bcc_BB|Cycle_YY|Type_ZZ|upper_path_Length_UU|C1_cov1|C2_cov2[...]|CN_covN|rank_RR
\end{Verbatim}
\end{small}
\begin{itemize}
\item \texttt{bcc\_BB} : Bi-connected component BB the event belongs to
\item \texttt{Cycle\_YY} : since in each bcc, several cycles may exist, this attribute indicates the ID of the cycle (here: cycle YY) that generated the bubble
\item \texttt{Type\_ZZ}: with \texttt{ZZ=\{0a, 0b, 1, 2, 3, 4\}}, the type also corresponds to the type given by the file name
\item \texttt{upper\_path\_Length\_UU} :  length (in nucleotides) of the sequence of the upper path of the bubble
\item \texttt{Cn}: with \texttt{n=\{1,..., N\}}: coverage of the path using reads from the  read file n; coverage is the raw count of reads mapping the path with at least \texttt{k} (i.e. the value specified for the $k-mer$ length)  nucleotides.
\item \texttt{rank\_RR} : %The rank is close to 1 if the alleles of the event are condition-specific. Otherwise, it is close to 0. Formally, the rank corresponds to the square of the Phi coefficient. In the case where there is one condition, the rank is always 0. In the case where there are more than two conditions, it is calculated for all pairs of conditions, and the maximal phi value is output. Note that the rank is just an indication of the strength of the association between the allele and a condition but this is not a statistical test. In particular, for low counts, the rank may be high but a statistical test may give a negative answer to the question ``is there an association between alleles and conditions ?''. On the other hand, for very large counts, a statistical test may give a positive answer while the effect is very small. The rank helps to measure the strength of the effect.
This piece of information used to give an indication of if the alleles/the variants were differentially expressed between two conditions. It was an indication of the strength of the association between the allele/the variant and a condition but this was not a statistical test.  We now provide a reliable indicator based on statistical analysis in our package kissDE (see \url{http://kissplice.prabi.fr/tools/kissDE/}).
\end{itemize}
The identifier for the lower path provides virtually the same information, but concerning the lower (shorter) path of the bubble. In order to facilitate the further post-processing of these files, the bcc, cycle, type and rank are repeated, although this information is redundant.

If \esoft is run with option \texttt{-r} (read files), the events in the output files are sorted with respect to their rank (this is not possible with option -g due to missing information about read coverage).

\subsection{Uncoherent Bubbles}

In order for a bubble to be considered as coherent, each nt has to be covered by at least one read.
Uncoherent bubbles are output in a separate file.
In principle, uncoherent bubbles should correspond to artefacts of the DBG (we lose information when we move from reads to $k-mers$).
In practice, real events which have a low coverage may produce uncoherent bubbles.
Hence, it may be worth to mine this file if you are interested in unfrequent events.

\section{Performances}

\subsection{Pre-treatment}

\esoft can be run on raw data. However, as any assembler, it performs better if the data is pre-treated.
Common pre-treatment includes polyA tail removal and quality filter.
These filters are not included in \esoft distribution but can easily be found. For instance, FASTX Toolkit can be used to trim the reads, i.e. removing the last bases for which quality is below a threshold (we commonly use 20). Reads shorter than 20 nt are then removed.

\subsection{Job calibration}

The first time you run \esoft, you may be interested in getting quickly initial results. For moderate size datasets (<500M reads), this can be achieved with default parameters. For large datasets (>500M reads), this may require to modify some parameters.

By default $c$ is set to 1. This means that only $k$-mers seen more than once are considered for the analysis. $k$-mers seen only once are indeed likely to correspond to sequencing errors. They can also correspond to rare variants. Increasing $c$ to 5 will correspond to a major speed-up of \esoft, at the expense of a loss in sensitivity: variants supported by less than 5 reads will not be reported anymore.

By default $k$ is set to 41. This means that repeats smaller than 41nt will not cause trouble. This also means that reads have to overlap by at least 40nt to be assembled. 
Increasing $k$ to 51 will enable to solve more complex regions due to repeats, and hence will speed-up \esoft, at the expense of a loss in sensitivity: reads will need to overlap by at least 50nt to be assembled.

You might be wondering about the disk space you need to have available for a job. \esoft writes in several files and temporary files, and we currently consider that you must have two to three times the size of your data in free space to run \esoft under proper conditions. To resolve disk space issues, we recommand to use \texttt{-d} option followed by a directory able to contain between two and three times the size of the input data.\\

If you want to use parallelisation option in your job, our advice is to put \texttt{-t} at 4 or 6 processors, more is not really efficient.



\subsection{Relative coverage}
The option \texttt{-C} edits the De-Bruijn graph constructed by removing edges non covered.
It is a local filter. For a specific node, if one of its outgoing edges is covered less than \texttt{C} (in percentage) it is removed.
For a small value of \texttt{C}, this option allows to remove artificial edges due to overlapping of two $k-mers$ (not supported by the reads) or sequencing errors. Increasing \texttt{-C} will reduce the run time as there will be less things to enumerate, but the price is to lose some events with a rare isoform.

\subsection{Big datasets}

%For some datasets, enumerating all bubbles is very long. Hence, we set a maximum amount of time (900s by default) that the algorithm spends in each BCC (biconnected component). If after this time, the BCC is not finished, the algorithm stops and moves to the next BCC. In practice, these unfinished BCCs correspond to very repetitive regions of the dataset (transposable elements...).
For some datasets, enumerating all bubbles is very long. Hence, we set a maximum amount of time (100000s by default) that the algorithm spends in each BCC (biconnected component). If after this time, the BCC is not finished, the algorithm stops and moves to the next BCC.
Running \esoft with \texttt{-u} option enables to output these unfinished BCCs for further inspection (for instance using Cytoscape).\\

If you intend to work on rather big datasets (> 1G reads), have a look at your \texttt{.e} log file in the end of the run. If you read "\texttt{Timeout reached}", it means the maximum amount of time has been reached, some bccs may have been lost then. We recommand to run again KisSplice with a higher \texttt{-C} (\texttt{-C} 0.05 is a good input) to simplify the graph (while not losing too much information as there are already many reads) and retrieve all events, or to increase the timeout (option \texttt{--timeout}).

%\subsection{Genome-size option}
%The \texttt{-z} option is used for a rough estimation of the graph size. It is an estimation of unique $k-mers$ contained into the dataset.
%A good evaluation of this parameter leads to an optimal graph construction step in term of memory consumption and time.
%On one hand, if the value is too low, the construction will take fewer memory but will take more time. On the other hand, if the value is too high, it will use a lot of memory.
%By default \texttt{z} is set to a billion (\texttt{1 000 000 000}) which is a good trade-off for most datasets.

\section{Further questions}
Please visit our at FAQ \url{http://kissplice.prabi.fr/FAQ/}.


%\bibliographystyle{unsrt}
%\bibliography{}
\end{document}
