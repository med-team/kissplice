#include "Utils.h"
#include <string>
#include <algorithm>
#include <iterator>

char complement(char n)
{
  switch(n)
  {
    case 'A':
      return 'T';
    case 'a':
      return 't';
    case 'T':
      return 'A';
    case 't':
      return 'a';
    case 'G':
      return 'C';
    case 'g':
      return 'c';
    case 'C':
      return 'G';
    case 'c':
      return 'g';
    default:
      return n;
  }
}

void reverse_complement(char *seq) {
  //get the complement
  std::string seqAsStr(seq);
  std::transform(
      seqAsStr.begin(),
      seqAsStr.end(),
      seqAsStr.begin(),
      complement);

  //reverse it
  std::string RC(seqAsStr.rbegin(), seqAsStr.rend());

  //put it in seq
  for (std::size_t i=0; i<RC.size(); i++)
    seq[i]=RC[i];
}

char invertStrand(char strand) {
  switch (strand) {
    case '+':
      return '-';
    case '-':
      return '+';
    default:
      return strand;
  }
}

void invertStrands(char * consensusStrandOfMappedReads, int size) {
  for (int i=0; i<size; i++)
    consensusStrandOfMappedReads[i]=invertStrand(consensusStrandOfMappedReads[i]);
}