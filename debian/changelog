kissplice (2.6.7-1) unstable; urgency=medium

  $ Team upload
  * Advertising repacking with +dfsg suffix for upcoming upstream versions
  * Shipping the Python file directly in /usr/bin, not symlinking to
    /usr/share/kissplice anymore
  * Adding internal binaries in a subdir of /usr/lib/${DEB_HOST_MULTIARCH}

  [ François Gindraud ]
  * Propagate upstream build system changes for bcalm and guide
  * Fix dh_auto_clean

  [ Andreas Tille ]
  * New upstream version
    Closes: #1073482, #1074658 
  * Standards-Version: 4.7.0 (routine-update)

 -- Pierre Gruet <pgt@debian.org>  Wed, 07 Aug 2024 17:30:12 +0200

kissplice (2.6.2-3) unstable; urgency=medium

  * Add build support for loongarch64
    Closes: #1058716
  * Standards-Version: 4.6.2 (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  Use secure URI in Homepage field.
  Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- Andreas Tille <tille@debian.org>  Tue, 09 Jan 2024 20:32:24 +0100

kissplice (2.6.2-2) unstable; urgency=medium

  * Team upload.

  [ Eric Long ]
  * Add build target for riscv64.
    Closes: #1027357

 -- Sascha Steinbiss <satta@debian.org>  Fri, 20 Jan 2023 19:37:26 +0100

kissplice (2.6.2-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 30 Jun 2022 16:27:19 +0200

kissplice (2.6.1-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Upstream moved to https://gitlab.inria.fr
  * New upstream version
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Build-Depends: texlive-latex-base

  [ Francois Gindraud ]
  * Update dependencies
  * Remove gcc10 patch
  * Replace redirection script with symlink to bcalm
  * New upstream version
  * Use generated doc and upsteam manpage

 -- Francois Gindraud <francois.gindraud@inria.fr>  Wed, 06 Apr 2022 16:19:49 +0200

kissplice (2.5.5+dfsg-1) unstable; urgency=medium

  * Disable Salsa-CI from building on i386
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 17 Feb 2022 19:16:12 +0100

kissplice (2.5.4+dfsg-1) unstable; urgency=medium

  * Remove bcalm source since there is a separate bcalm package (see #962085)
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Cleanup rules from references to bcalm and means to build gatb-core

 -- Andreas Tille <tille@debian.org>  Mon, 13 Dec 2021 08:21:05 +0100

kissplice (2.5.3-3) unstable; urgency=medium

  * Team Upload.
  * Fix gcc-10 FTBFS (Closes: #972768)

 -- Nilesh Patra <npatra974@gmail.com>  Mon, 26 Oct 2020 04:00:37 +0530

kissplice (2.5.3-2) unstable; urgency=medium

  * Restore build time tests: fixed upstream (Closes: #936798)
  * Fix bcalm install (Closes: #962085)

 -- David Parsons <david.parsons@inria.fr>  Tue, 23 Jun 2020 16:27:59 +0200

kissplice (2.5.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version.
  * Improved clean post build.

 -- Steffen Moeller <moeller@debian.org>  Thu, 18 Jun 2020 18:47:58 +0200

kissplice (2.5.1-1) unstable; urgency=medium

  * Team upload.

  [ Steffen Möller ]
  * Added ref to bioconda

  [ Antoni Villalonga ]
  * New upstream version 2.5.1
  * Python2 to Python3 (Closes: 936798)
  * tests-2to3.patch: tests migration from Python2 to 3
  * spelling.patch: Amend spelling typos
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Standards-Version: 4.5.0

  [ Nilesh Patra ]
  * Fix and add autopkgtests

  [ Andreas Tille ]
  * Add debian/TODO linking to build time errors
  * Remove extra license file

 -- Nilesh Patra <npatra974@gmail.com>  Fri, 29 May 2020 02:55:28 +0530

kissplice (2.4.0-p1-5) unstable; urgency=medium

  * Team upload.

  [ Saira Hussain ]
  * Add autopkgtest

  [ Andreas Tille ]
  * debhelper-compat 12
  * Standards-Version: 4.4.0

 -- Saira Hussain <saira.h0213@zoho.com>  Mon, 12 Aug 2019 12:07:18 +0100

kissplice (2.4.0-p1-4) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Use secure copyright file specification URI.

  [ Andreas Tille ]
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Tue, 08 Jan 2019 12:48:29 +0100

kissplice (2.4.0-p1-3) unstable; urgency=medium

  * Add dh-python to Build-Depends since this will be dropped from
    python-dev's Depends soon
  * Standards-Version: 4.1.3
  * debhelper 11
  * do not parse d/changelog
  * d/watch: Checked that https does not work

 -- Andreas Tille <tille@debian.org>  Mon, 26 Mar 2018 17:15:22 +0200

kissplice (2.4.0-p1-2) unstable; urgency=medium

  [ Steffen Moeller ]
  * debian/upstream/metadata:
    - yamllint cleanliness
    - Added references to registries
  * debian/copyright: corrected Upstream-Name spelling

  [ Andreas Tille ]
  * Moved packaging from SVN to Git
  * debhelper 10
  * Standards-Version: 4.1.1

 -- Andreas Tille <tille@debian.org>  Wed, 15 Nov 2017 14:19:37 +0100

kissplice (2.4.0-p1-1) unstable; urgency=medium

  [ David Parsons ]
  * New upstream version

  [ Steffen Moeller ]
  * Introduced EDAM ontology description to debian/upstream/edam.

  [ Andreas Tille ]
  * hardening=+all
  * kissplice is Python but not Python3
  * make proper use of dh_python
  * cme fix dpkg-control

 -- David Parsons <david.parsons@inria.fr>  Thu, 01 Sep 2016 13:30:59 +0200

kissplice (2.3.1-1) unstable; urgency=medium

  * New upstream version
  * Force installation of secondary binaries in lib rather than libexec

 -- David Parsons <david.parsons@inria.fr>  Wed, 22 Apr 2015 16:26:53 +0200

kissplice (2.2.1-3) unstable; urgency=medium

  * Architecture attribute in a single line
    Closes: #780473

 -- Andreas Tille <tille@debian.org>  Sat, 14 Mar 2015 22:37:41 +0100

kissplice (2.2.1-2) unstable; urgency=medium

  [ David Parsons ]
  * Restrict architectures to 64Bit only

  [ Andreas Tille ]
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Fri, 10 Oct 2014 11:03:43 +0200

kissplice (2.2.1-1) unstable; urgency=medium

  * New upstream version
  * Fix watch

 -- David Parsons <david.parsons@inria.fr>  Tue, 26 Aug 2014 14:53:50 +0200

kissplice (2.1.0-1) unstable; urgency=medium

  * New upstream version
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Fri, 31 Jan 2014 18:13:40 +0100

kissplice (1.8.3-1) unstable; urgency=low

  * Initial debian release (Closes: #711946)

 -- David Parsons <david.parsons@inria.fr>  Wed, 05 Jun 2013 12:00:35 +0200
